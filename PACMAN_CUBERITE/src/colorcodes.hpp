#ifndef COLORCODES_HPP_INCLUDED
#define COLORCODES_HPP_INCLUDED

#define COL_RED         sf::Color(  255,   0,   0, 255)
#define COL_WHITE       sf::Color(  255, 255, 255, 255)
#define COL_BLUE        sf::Color(    0,   0, 255, 255)
#define COL_GREEN       sf::Color(    0, 255,   0, 255)
#define COL_CYAN        sf::Color(    0, 128, 255, 255)
#define COL_YELLOW      sf::Color(  255, 255,   0, 255)
#define TXT_REG         sf::Text::Regular
#define COL_WHITE_T     sf::Color(  255, 255, 255, 0)
#define COL_WHITE_T50   sf::Color(  255, 255, 255, 192)

#endif // COLORCODES_HPP_INCLUDED
