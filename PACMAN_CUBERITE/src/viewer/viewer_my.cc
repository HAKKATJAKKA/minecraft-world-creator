//Make things easy, so things get complicated

#include "viewer_my_inits.h"

#include "viewer_my_image_loader.h"

#include "viewer_my_inits_more.h"

#include "viewer_my_loadobj_functions.h"

#include "viewer_my_draw.h"

#include "viewer_my_loadobj.h"

#include "viewer_my_sfmlviewer.h"

#include "viewer_my_get3d.h"

#include "viewer_my_get_draw.h"

//#include "test_data.hpp"

#include "viewer_my_recording.hpp"

#include "viewer_my_todo.hpp"

#include "viewer_my_move_recorder.hpp"

#include "viewer_my_zoomtest.hpp"

#include "viewer_my_make_frustum.hpp"

#include "viewer_my_plane_stuff.hpp"

#include "viewer_my_divers.hpp"

