# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "D:/PACMAN/fly/extlibs/src/simplexnoise.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/extlibs/src/simplexnoise.cpp.obj"
  "D:/PACMAN/fly/src/Airplane.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Airplane.cpp.obj"
  "D:/PACMAN/fly/src/BoundingBox.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/BoundingBox.cpp.obj"
  "D:/PACMAN/fly/src/Camera.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Camera.cpp.obj"
  "D:/PACMAN/fly/src/CameraController.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/CameraController.cpp.obj"
  "D:/PACMAN/fly/src/Controller.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Controller.cpp.obj"
  "D:/PACMAN/fly/src/Debug/Box.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Debug/Box.cpp.obj"
  "D:/PACMAN/fly/src/FrameBuffer.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/FrameBuffer.cpp.obj"
  "D:/PACMAN/fly/src/Log.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Log.cpp.obj"
  "D:/PACMAN/fly/src/Model.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Model.cpp.obj"
  "D:/PACMAN/fly/src/ParticleData.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/ParticleData.cpp.obj"
  "D:/PACMAN/fly/src/ParticleEmitters.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/ParticleEmitters.cpp.obj"
  "D:/PACMAN/fly/src/ParticleRenderer.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/ParticleRenderer.cpp.obj"
  "D:/PACMAN/fly/src/ParticleSystem.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/ParticleSystem.cpp.obj"
  "D:/PACMAN/fly/src/ParticleUpdaters.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/ParticleUpdaters.cpp.obj"
  "D:/PACMAN/fly/src/Shader.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Shader.cpp.obj"
  "D:/PACMAN/fly/src/ShadowMap.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/ShadowMap.cpp.obj"
  "D:/PACMAN/fly/src/Sky.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Sky.cpp.obj"
  "D:/PACMAN/fly/src/Sprite.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Sprite.cpp.obj"
  "D:/PACMAN/fly/src/Terrain.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Terrain.cpp.obj"
  "D:/PACMAN/fly/src/TerrainRenderer.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/TerrainRenderer.cpp.obj"
  "D:/PACMAN/fly/src/TextureManager.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/TextureManager.cpp.obj"
  "D:/PACMAN/fly/src/Utility.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/Utility.cpp.obj"
  "D:/PACMAN/fly/src/main.cpp" "D:/PACMAN/fly/build/CMakeFiles/Fly.dir/src/main.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "SFML_STATIC"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../extlibs"
  "D:/PACMAN/PACMAN_CUBERITE/src/include"
  "D:/PACMAN/glew-2.1.0/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
