return
{
	TakeDamageInfo =
	{
		Variables =
		{
			Attacker =
			{
				Type = "cEntity",
			},
			DamageType =
			{
				Type = "eDamageType",
			},
			FinalDamage =
			{
				Type = "float",
			},
			Knockback =
			{
				Type = "Vector3<double>",
			},
			RawDamage =
			{
				Type = "int",
			},
		},
	},
}
