return
{
	cPainting =
	{
		Inherits =
		{
			"cHangingEntity",
		},
		Functions =
		{
			GetName =
			{
				{
					Params =
					{
					},
					Returns =
					{
						{
							Type = "const AString",
						},
					},
					Desc = "Returns the protocol name of the painting",
				},
			},
		},
	},
}
