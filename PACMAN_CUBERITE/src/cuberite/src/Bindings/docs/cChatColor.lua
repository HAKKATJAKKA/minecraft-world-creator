return
{
	cChatColor =
	{
		Constants =
		{
			Black =
			{
				Type = "const char*",
			},
			Blue =
			{
				Type = "const char*",
			},
			Bold =
			{
				Type = "const char*",
			},
			Color =
			{
				Type = "const char*",
				Desc = "@deprecated use ChatColor::Delimiter instead",
			},
			DarkPurple =
			{
				Type = "const char*",
			},
			Delimiter =
			{
				Type = "const char*",
			},
			Gold =
			{
				Type = "const char*",
			},
			Gray =
			{
				Type = "const char*",
			},
			Green =
			{
				Type = "const char*",
			},
			Italic =
			{
				Type = "const char*",
			},
			LightBlue =
			{
				Type = "const char*",
			},
			LightGray =
			{
				Type = "const char*",
			},
			LightGreen =
			{
				Type = "const char*",
			},
			LightPurple =
			{
				Type = "const char*",
			},
			Navy =
			{
				Type = "const char*",
			},
			Plain =
			{
				Type = "const char*",
			},
			Purple =
			{
				Type = "const char*",
			},
			Random =
			{
				Type = "const char*",
			},
			Red =
			{
				Type = "const char*",
			},
			Rose =
			{
				Type = "const char*",
			},
			Strikethrough =
			{
				Type = "const char*",
			},
			Underlined =
			{
				Type = "const char*",
			},
			White =
			{
				Type = "const char*",
			},
			Yellow =
			{
				Type = "const char*",
			},
		},
	},
}
