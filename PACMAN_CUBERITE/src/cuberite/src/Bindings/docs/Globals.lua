return
{
	Globals =
	{
		Enums =
		{
			unnamedEnum_1 =
			{
				{
					Name = "MAX_EXPERIENCE_ORB_SIZE",
				},
			},
			unnamedEnum_2 =
			{
				{
					Name = "DIG_STATUS_CANCELLED",
				},
				{
					Name = "DIG_STATUS_DROP_HELD",
				},
				{
					Name = "DIG_STATUS_DROP_STACK",
				},
				{
					Name = "DIG_STATUS_FINISHED",
				},
				{
					Name = "DIG_STATUS_SHOOT_EAT",
				},
				{
					Name = "DIG_STATUS_STARTED",
				},
				{
					Name = "DIG_STATUS_SWAP_ITEM_IN_HAND",
				},
			},
			EMCSBiome =
			{
				{
					Name = "biBeach",
				},
				{
					Name = "biBirchForest",
				},
				{
					Name = "biBirchForestHills",
				},
				{
					Name = "biBirchForestHillsM",
				},
				{
					Name = "biBirchForestM",
				},
				{
					Name = "biColdBeach",
				},
				{
					Name = "biColdTaiga",
				},
				{
					Name = "biColdTaigaHills",
				},
				{
					Name = "biColdTaigaM",
				},
				{
					Name = "biDeepOcean",
				},
				{
					Name = "biDesert",
				},
				{
					Name = "biDesertHills",
				},
				{
					Name = "biDesertM",
				},
				{
					Name = "biEnd",
				},
				{
					Name = "biExtremeHills",
				},
				{
					Name = "biExtremeHillsEdge",
				},
				{
					Name = "biExtremeHillsM",
				},
				{
					Name = "biExtremeHillsPlus",
				},
				{
					Name = "biExtremeHillsPlusM",
				},
				{
					Name = "biFirstBiome",
				},
				{
					Name = "biFirstVariantBiome",
				},
				{
					Name = "biFlowerForest",
				},
				{
					Name = "biForest",
				},
				{
					Name = "biForestHills",
				},
				{
					Name = "biFrozenOcean",
				},
				{
					Name = "biFrozenRiver",
				},
				{
					Name = "biHell",
				},
				{
					Name = "biIceMountains",
				},
				{
					Name = "biIcePlains",
				},
				{
					Name = "biIcePlainsSpikes",
				},
				{
					Name = "biInvalidBiome",
				},
				{
					Name = "biJungle",
				},
				{
					Name = "biJungleEdge",
				},
				{
					Name = "biJungleEdgeM",
				},
				{
					Name = "biJungleHills",
				},
				{
					Name = "biJungleM",
				},
				{
					Name = "biMaxBiome",
				},
				{
					Name = "biMaxVariantBiome",
				},
				{
					Name = "biMegaSpruceTaiga",
				},
				{
					Name = "biMegaSpruceTaigaHills",
				},
				{
					Name = "biMegaTaiga",
				},
				{
					Name = "biMegaTaigaHills",
				},
				{
					Name = "biMesa",
				},
				{
					Name = "biMesaBryce",
				},
				{
					Name = "biMesaPlateau",
				},
				{
					Name = "biMesaPlateauF",
				},
				{
					Name = "biMesaPlateauFM",
				},
				{
					Name = "biMesaPlateauM",
				},
				{
					Name = "biMushroomIsland",
				},
				{
					Name = "biMushroomShore",
				},
				{
					Name = "biNether",
				},
				{
					Name = "biNumBiomes",
				},
				{
					Name = "biNumVariantBiomes",
				},
				{
					Name = "biOcean",
				},
				{
					Name = "biPlains",
				},
				{
					Name = "biRiver",
				},
				{
					Name = "biRoofedForest",
				},
				{
					Name = "biRoofedForestM",
				},
				{
					Name = "biSavanna",
				},
				{
					Name = "biSavannaM",
				},
				{
					Name = "biSavannaPlateau",
				},
				{
					Name = "biSavannaPlateauM",
				},
				{
					Name = "biSky",
				},
				{
					Name = "biStoneBeach",
				},
				{
					Name = "biSunflowerPlains",
				},
				{
					Name = "biSwampland",
				},
				{
					Name = "biSwamplandM",
				},
				{
					Name = "biTaiga",
				},
				{
					Name = "biTaigaHills",
				},
				{
					Name = "biTaigaM",
				},
				{
					Name = "biTundra",
				},
				{
					Name = "biVariant",
				},
			},
			ENUM_BLOCK_ID =
			{
				{
					Name = "E_BLOCK_ACACIA_DOOR",
				},
				{
					Name = "E_BLOCK_ACACIA_FENCE",
				},
				{
					Name = "E_BLOCK_ACACIA_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_ACACIA_WOOD_STAIRS",
				},
				{
					Name = "E_BLOCK_ACTIVATOR_RAIL",
				},
				{
					Name = "E_BLOCK_ACTIVE_COMPARATOR",
				},
				{
					Name = "E_BLOCK_AIR",
				},
				{
					Name = "E_BLOCK_ANVIL",
				},
				{
					Name = "E_BLOCK_BARRIER",
				},
				{
					Name = "E_BLOCK_BEACON",
				},
				{
					Name = "E_BLOCK_BED",
				},
				{
					Name = "E_BLOCK_BEDROCK",
				},
				{
					Name = "E_BLOCK_BEETROOTS",
				},
				{
					Name = "E_BLOCK_BIG_FLOWER",
				},
				{
					Name = "E_BLOCK_BIRCH_DOOR",
				},
				{
					Name = "E_BLOCK_BIRCH_FENCE",
				},
				{
					Name = "E_BLOCK_BIRCH_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_BIRCH_WOOD_STAIRS",
				},
				{
					Name = "E_BLOCK_BLACK_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_BLACK_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_BLOCK_OF_COAL",
				},
				{
					Name = "E_BLOCK_BLOCK_OF_REDSTONE",
				},
				{
					Name = "E_BLOCK_BLUE_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_BLUE_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_BONE_BLOCK",
				},
				{
					Name = "E_BLOCK_BOOKCASE",
				},
				{
					Name = "E_BLOCK_BREWING_STAND",
				},
				{
					Name = "E_BLOCK_BRICK",
				},
				{
					Name = "E_BLOCK_BRICK_STAIRS",
				},
				{
					Name = "E_BLOCK_BROWN_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_BROWN_MUSHROOM",
				},
				{
					Name = "E_BLOCK_BROWN_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_BURNING_FURNACE",
				},
				{
					Name = "E_BLOCK_CACTUS",
				},
				{
					Name = "E_BLOCK_CAKE",
				},
				{
					Name = "E_BLOCK_CARPET",
				},
				{
					Name = "E_BLOCK_CARROTS",
				},
				{
					Name = "E_BLOCK_CAULDRON",
				},
				{
					Name = "E_BLOCK_CHAIN_COMMAND_BLOCK",
				},
				{
					Name = "E_BLOCK_CHEST",
				},
				{
					Name = "E_BLOCK_CHORUS_FLOWER",
				},
				{
					Name = "E_BLOCK_CHORUS_PLANT",
				},
				{
					Name = "E_BLOCK_CLAY",
				},
				{
					Name = "E_BLOCK_COAL_ORE",
				},
				{
					Name = "E_BLOCK_COBBLESTONE",
				},
				{
					Name = "E_BLOCK_COBBLESTONE_STAIRS",
				},
				{
					Name = "E_BLOCK_COBBLESTONE_WALL",
				},
				{
					Name = "E_BLOCK_COBWEB",
				},
				{
					Name = "E_BLOCK_COCOA_POD",
				},
				{
					Name = "E_BLOCK_COMMAND_BLOCK",
				},
				{
					Name = "E_BLOCK_CONCRETE",
				},
				{
					Name = "E_BLOCK_CONCRETE_POWDER",
				},
				{
					Name = "E_BLOCK_CRAFTING_TABLE",
				},
				{
					Name = "E_BLOCK_CROPS",
				},
				{
					Name = "E_BLOCK_CYAN_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_CYAN_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_DANDELION",
				},
				{
					Name = "E_BLOCK_DARK_OAK_DOOR",
				},
				{
					Name = "E_BLOCK_DARK_OAK_FENCE",
				},
				{
					Name = "E_BLOCK_DARK_OAK_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_DARK_OAK_WOOD_STAIRS",
				},
				{
					Name = "E_BLOCK_DAYLIGHT_SENSOR",
				},
				{
					Name = "E_BLOCK_DEAD_BUSH",
				},
				{
					Name = "E_BLOCK_DETECTOR_RAIL",
				},
				{
					Name = "E_BLOCK_DIAMOND_BLOCK",
				},
				{
					Name = "E_BLOCK_DIAMOND_ORE",
				},
				{
					Name = "E_BLOCK_DIRT",
				},
				{
					Name = "E_BLOCK_DISPENSER",
				},
				{
					Name = "E_BLOCK_DOUBLE_RED_SANDSTONE_SLAB",
				},
				{
					Name = "E_BLOCK_DOUBLE_STONE_SLAB",
				},
				{
					Name = "E_BLOCK_DOUBLE_WOODEN_SLAB",
				},
				{
					Name = "E_BLOCK_DRAGON_EGG",
				},
				{
					Name = "E_BLOCK_DROPPER",
				},
				{
					Name = "E_BLOCK_EMERALD_BLOCK",
				},
				{
					Name = "E_BLOCK_EMERALD_ORE",
				},
				{
					Name = "E_BLOCK_ENCHANTMENT_TABLE",
				},
				{
					Name = "E_BLOCK_ENDER_CHEST",
				},
				{
					Name = "E_BLOCK_END_BRICKS",
				},
				{
					Name = "E_BLOCK_END_GATEWAY",
				},
				{
					Name = "E_BLOCK_END_PORTAL",
				},
				{
					Name = "E_BLOCK_END_PORTAL_FRAME",
				},
				{
					Name = "E_BLOCK_END_ROD",
				},
				{
					Name = "E_BLOCK_END_STONE",
				},
				{
					Name = "E_BLOCK_FARMLAND",
				},
				{
					Name = "E_BLOCK_FENCE",
				},
				{
					Name = "E_BLOCK_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_FIRE",
				},
				{
					Name = "E_BLOCK_FLOWER",
				},
				{
					Name = "E_BLOCK_FLOWER_POT",
				},
				{
					Name = "E_BLOCK_FROSTED_ICE",
				},
				{
					Name = "E_BLOCK_FURNACE",
				},
				{
					Name = "E_BLOCK_GLASS",
				},
				{
					Name = "E_BLOCK_GLASS_PANE",
				},
				{
					Name = "E_BLOCK_GLOWSTONE",
				},
				{
					Name = "E_BLOCK_GOLD_BLOCK",
				},
				{
					Name = "E_BLOCK_GOLD_ORE",
				},
				{
					Name = "E_BLOCK_GRASS",
				},
				{
					Name = "E_BLOCK_GRASS_PATH",
				},
				{
					Name = "E_BLOCK_GRAVEL",
				},
				{
					Name = "E_BLOCK_GRAY_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_GRAY_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_GREEN_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_GREEN_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_HARDENED_CLAY",
				},
				{
					Name = "E_BLOCK_HAY_BALE",
				},
				{
					Name = "E_BLOCK_HEAD",
				},
				{
					Name = "E_BLOCK_HEAVY_WEIGHTED_PRESSURE_PLATE",
				},
				{
					Name = "E_BLOCK_HOPPER",
				},
				{
					Name = "E_BLOCK_HUGE_BROWN_MUSHROOM",
				},
				{
					Name = "E_BLOCK_HUGE_RED_MUSHROOM",
				},
				{
					Name = "E_BLOCK_ICE",
				},
				{
					Name = "E_BLOCK_INACTIVE_COMPARATOR",
				},
				{
					Name = "E_BLOCK_INVERTED_DAYLIGHT_SENSOR",
				},
				{
					Name = "E_BLOCK_IRON_BARS",
				},
				{
					Name = "E_BLOCK_IRON_BLOCK",
				},
				{
					Name = "E_BLOCK_IRON_DOOR",
				},
				{
					Name = "E_BLOCK_IRON_ORE",
				},
				{
					Name = "E_BLOCK_IRON_TRAPDOOR",
				},
				{
					Name = "E_BLOCK_JACK_O_LANTERN",
				},
				{
					Name = "E_BLOCK_JUKEBOX",
				},
				{
					Name = "E_BLOCK_JUNGLE_DOOR",
				},
				{
					Name = "E_BLOCK_JUNGLE_FENCE",
				},
				{
					Name = "E_BLOCK_JUNGLE_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_JUNGLE_WOOD_STAIRS",
				},
				{
					Name = "E_BLOCK_LADDER",
				},
				{
					Name = "E_BLOCK_LAPIS_BLOCK",
				},
				{
					Name = "E_BLOCK_LAPIS_ORE",
				},
				{
					Name = "E_BLOCK_LAVA",
				},
				{
					Name = "E_BLOCK_LEAVES",
				},
				{
					Name = "E_BLOCK_LEVER",
				},
				{
					Name = "E_BLOCK_LIGHT_BLUE_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_LIGHT_BLUE_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_LIGHT_GRAY_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_LIGHT_GRAY_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_LIGHT_WEIGHTED_PRESSURE_PLATE",
				},
				{
					Name = "E_BLOCK_LILY_PAD",
				},
				{
					Name = "E_BLOCK_LIME_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_LIME_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_LIT_FURNACE",
				},
				{
					Name = "E_BLOCK_LOG",
				},
				{
					Name = "E_BLOCK_MAGENTA_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_MAGENTA_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_MAGMA",
				},
				{
					Name = "E_BLOCK_MAX_TYPE_ID",
					Desc = "Maximum BlockType number used",
				},
				{
					Name = "E_BLOCK_MELON",
				},
				{
					Name = "E_BLOCK_MELON_STEM",
				},
				{
					Name = "E_BLOCK_MINECART_TRACKS",
				},
				{
					Name = "E_BLOCK_MOB_SPAWNER",
				},
				{
					Name = "E_BLOCK_MOSSY_COBBLESTONE",
				},
				{
					Name = "E_BLOCK_MYCELIUM",
				},
				{
					Name = "E_BLOCK_NETHERRACK",
				},
				{
					Name = "E_BLOCK_NETHER_BRICK",
				},
				{
					Name = "E_BLOCK_NETHER_BRICK_FENCE",
				},
				{
					Name = "E_BLOCK_NETHER_BRICK_STAIRS",
				},
				{
					Name = "E_BLOCK_NETHER_PORTAL",
				},
				{
					Name = "E_BLOCK_NETHER_QUARTZ_ORE",
				},
				{
					Name = "E_BLOCK_NETHER_WART",
				},
				{
					Name = "E_BLOCK_NETHER_WART_BLOCK",
				},
				{
					Name = "E_BLOCK_NEW_LEAVES",
				},
				{
					Name = "E_BLOCK_NEW_LOG",
				},
				{
					Name = "E_BLOCK_NOTE_BLOCK",
				},
				{
					Name = "E_BLOCK_NUMBER_OF_TYPES",
					Desc = "Number of individual (different) blocktypes",
				},
				{
					Name = "E_BLOCK_OAK_DOOR",
				},
				{
					Name = "E_BLOCK_OAK_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_OAK_WOOD_STAIRS",
				},
				{
					Name = "E_BLOCK_OBSERVER",
				},
				{
					Name = "E_BLOCK_OBSIDIAN",
				},
				{
					Name = "E_BLOCK_ORANGE_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_ORANGE_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_PACKED_ICE",
				},
				{
					Name = "E_BLOCK_PINK_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_PINK_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_PISTON",
				},
				{
					Name = "E_BLOCK_PISTON_EXTENSION",
				},
				{
					Name = "E_BLOCK_PISTON_MOVED_BLOCK",
				},
				{
					Name = "E_BLOCK_PLANKS",
				},
				{
					Name = "E_BLOCK_POTATOES",
				},
				{
					Name = "E_BLOCK_POWERED_RAIL",
				},
				{
					Name = "E_BLOCK_PRISMARINE_BLOCK",
				},
				{
					Name = "E_BLOCK_PUMPKIN",
				},
				{
					Name = "E_BLOCK_PUMPKIN_STEM",
				},
				{
					Name = "E_BLOCK_PURPLE_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_PURPLE_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_PURPUR_BLOCK",
				},
				{
					Name = "E_BLOCK_PURPUR_DOUBLE_SLAB",
				},
				{
					Name = "E_BLOCK_PURPUR_PILLAR",
				},
				{
					Name = "E_BLOCK_PURPUR_SLAB",
				},
				{
					Name = "E_BLOCK_PURPUR_STAIRS",
				},
				{
					Name = "E_BLOCK_QUARTZ_BLOCK",
				},
				{
					Name = "E_BLOCK_QUARTZ_STAIRS",
				},
				{
					Name = "E_BLOCK_RAIL",
				},
				{
					Name = "E_BLOCK_REDSTONE_LAMP_OFF",
				},
				{
					Name = "E_BLOCK_REDSTONE_LAMP_ON",
				},
				{
					Name = "E_BLOCK_REDSTONE_ORE",
				},
				{
					Name = "E_BLOCK_REDSTONE_ORE_GLOWING",
				},
				{
					Name = "E_BLOCK_REDSTONE_REPEATER_OFF",
				},
				{
					Name = "E_BLOCK_REDSTONE_REPEATER_ON",
				},
				{
					Name = "E_BLOCK_REDSTONE_TORCH_OFF",
				},
				{
					Name = "E_BLOCK_REDSTONE_TORCH_ON",
				},
				{
					Name = "E_BLOCK_REDSTONE_WIRE",
				},
				{
					Name = "E_BLOCK_RED_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_RED_MUSHROOM",
				},
				{
					Name = "E_BLOCK_RED_NETHER_BRICK",
				},
				{
					Name = "E_BLOCK_RED_ROSE",
				},
				{
					Name = "E_BLOCK_RED_SANDSTONE",
				},
				{
					Name = "E_BLOCK_RED_SANDSTONE_SLAB",
				},
				{
					Name = "E_BLOCK_RED_SANDSTONE_STAIRS",
				},
				{
					Name = "E_BLOCK_RED_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_REEDS",
				},
				{
					Name = "E_BLOCK_REPEATING_COMMAND_BLOCK",
				},
				{
					Name = "E_BLOCK_SAND",
				},
				{
					Name = "E_BLOCK_SANDSTONE",
				},
				{
					Name = "E_BLOCK_SANDSTONE_STAIRS",
				},
				{
					Name = "E_BLOCK_SAPLING",
				},
				{
					Name = "E_BLOCK_SEA_LANTERN",
				},
				{
					Name = "E_BLOCK_SIGN_POST",
				},
				{
					Name = "E_BLOCK_SILVERFISH_EGG",
				},
				{
					Name = "E_BLOCK_SLIME_BLOCK",
				},
				{
					Name = "E_BLOCK_SNOW",
				},
				{
					Name = "E_BLOCK_SNOW_BLOCK",
				},
				{
					Name = "E_BLOCK_SOULSAND",
				},
				{
					Name = "E_BLOCK_SPONGE",
				},
				{
					Name = "E_BLOCK_SPRUCE_DOOR",
				},
				{
					Name = "E_BLOCK_SPRUCE_FENCE",
				},
				{
					Name = "E_BLOCK_SPRUCE_FENCE_GATE",
				},
				{
					Name = "E_BLOCK_SPRUCE_WOOD_STAIRS",
				},
				{
					Name = "E_BLOCK_STAINED_CLAY",
				},
				{
					Name = "E_BLOCK_STAINED_GLASS",
				},
				{
					Name = "E_BLOCK_STAINED_GLASS_PANE",
				},
				{
					Name = "E_BLOCK_STANDING_BANNER",
				},
				{
					Name = "E_BLOCK_STATIONARY_LAVA",
				},
				{
					Name = "E_BLOCK_STATIONARY_WATER",
				},
				{
					Name = "E_BLOCK_STICKY_PISTON",
				},
				{
					Name = "E_BLOCK_STONE",
				},
				{
					Name = "E_BLOCK_STONE_BRICKS",
				},
				{
					Name = "E_BLOCK_STONE_BRICK_STAIRS",
				},
				{
					Name = "E_BLOCK_STONE_BUTTON",
				},
				{
					Name = "E_BLOCK_STONE_PRESSURE_PLATE",
				},
				{
					Name = "E_BLOCK_STONE_SLAB",
				},
				{
					Name = "E_BLOCK_STRUCTURE_BLOCK",
				},
				{
					Name = "E_BLOCK_STRUCTURE_VOID",
				},
				{
					Name = "E_BLOCK_SUGARCANE",
				},
				{
					Name = "E_BLOCK_TALL_GRASS",
				},
				{
					Name = "E_BLOCK_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_TNT",
				},
				{
					Name = "E_BLOCK_TORCH",
				},
				{
					Name = "E_BLOCK_TRAPDOOR",
				},
				{
					Name = "E_BLOCK_TRAPPED_CHEST",
				},
				{
					Name = "E_BLOCK_TRIPWIRE",
				},
				{
					Name = "E_BLOCK_TRIPWIRE_HOOK",
				},
				{
					Name = "E_BLOCK_UNFINISHED",
				},
				{
					Name = "E_BLOCK_VINES",
				},
				{
					Name = "E_BLOCK_WALLSIGN",
				},
				{
					Name = "E_BLOCK_WALL_BANNER",
				},
				{
					Name = "E_BLOCK_WATER",
				},
				{
					Name = "E_BLOCK_WHITE_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_WHITE_SHULKER_BOX",
				},
				{
					Name = "E_BLOCK_WOODEN_BUTTON",
				},
				{
					Name = "E_BLOCK_WOODEN_DOOR",
				},
				{
					Name = "E_BLOCK_WOODEN_PRESSURE_PLATE",
				},
				{
					Name = "E_BLOCK_WOODEN_SLAB",
				},
				{
					Name = "E_BLOCK_WOODEN_STAIRS",
				},
				{
					Name = "E_BLOCK_WOOL",
				},
				{
					Name = "E_BLOCK_WORKBENCH",
				},
				{
					Name = "E_BLOCK_YELLOW_FLOWER",
				},
				{
					Name = "E_BLOCK_YELLOW_GLAZED_TERRACOTTA",
				},
				{
					Name = "E_BLOCK_YELLOW_SHULKER_BOX",
				},
			},
			ENUM_BLOCK_META =
			{
				{
					Name = "E_BLOCK_ANVIL_HIGH_DAMAGE",
				},
				{
					Name = "E_BLOCK_ANVIL_LOW_DAMAGE",
				},
				{
					Name = "E_BLOCK_ANVIL_NO_DAMAGE",
				},
				{
					Name = "E_BLOCK_ANVIL_X",
				},
				{
					Name = "E_BLOCK_ANVIL_Z",
				},
				{
					Name = "E_BLOCK_BED_BED_HEAD",
				},
				{
					Name = "E_BLOCK_BED_OCCUPIED",
				},
				{
					Name = "E_BLOCK_BED_XM",
				},
				{
					Name = "E_BLOCK_BED_XP",
				},
				{
					Name = "E_BLOCK_BED_ZM",
				},
				{
					Name = "E_BLOCK_BED_ZP",
				},
				{
					Name = "E_BLOCK_BUTTON_PRESSED",
				},
				{
					Name = "E_BLOCK_BUTTON_XM",
				},
				{
					Name = "E_BLOCK_BUTTON_XP",
				},
				{
					Name = "E_BLOCK_BUTTON_YM",
				},
				{
					Name = "E_BLOCK_BUTTON_YP",
				},
				{
					Name = "E_BLOCK_BUTTON_ZM",
				},
				{
					Name = "E_BLOCK_BUTTON_ZP",
				},
				{
					Name = "E_BLOCK_STAIRS_UPSIDE_DOWN",
				},
				{
					Name = "E_BLOCK_STAIRS_XM",
				},
				{
					Name = "E_BLOCK_STAIRS_XP",
				},
				{
					Name = "E_BLOCK_STAIRS_ZM",
				},
				{
					Name = "E_BLOCK_STAIRS_ZP",
				},
				{
					Name = "E_META_BIG_FLOWER_DOUBLE_TALL_GRASS",
				},
				{
					Name = "E_META_BIG_FLOWER_LARGE_FERN",
				},
				{
					Name = "E_META_BIG_FLOWER_LILAC",
				},
				{
					Name = "E_META_BIG_FLOWER_PEONY",
				},
				{
					Name = "E_META_BIG_FLOWER_ROSE_BUSH",
				},
				{
					Name = "E_META_BIG_FLOWER_SUNFLOWER",
				},
				{
					Name = "E_META_BIG_FLOWER_TOP",
				},
				{
					Name = "E_META_BREWING_STAND_FILLED_SLOT_XM_ZM",
				},
				{
					Name = "E_META_BREWING_STAND_FILLED_SLOT_XM_ZP",
				},
				{
					Name = "E_META_BREWING_STAND_FILLED_SLOT_XP",
				},
				{
					Name = "E_META_CARPET_BLACK",
				},
				{
					Name = "E_META_CARPET_BLUE",
				},
				{
					Name = "E_META_CARPET_BROWN",
				},
				{
					Name = "E_META_CARPET_CYAN",
				},
				{
					Name = "E_META_CARPET_GRAY",
				},
				{
					Name = "E_META_CARPET_GREEN",
				},
				{
					Name = "E_META_CARPET_LIGHTBLUE",
				},
				{
					Name = "E_META_CARPET_LIGHTGRAY",
				},
				{
					Name = "E_META_CARPET_LIGHTGREEN",
				},
				{
					Name = "E_META_CARPET_MAGENTA",
				},
				{
					Name = "E_META_CARPET_ORANGE",
				},
				{
					Name = "E_META_CARPET_PINK",
				},
				{
					Name = "E_META_CARPET_PURPLE",
				},
				{
					Name = "E_META_CARPET_RED",
				},
				{
					Name = "E_META_CARPET_WHITE",
				},
				{
					Name = "E_META_CARPET_YELLOW",
				},
				{
					Name = "E_META_CHEST_FACING_XM",
				},
				{
					Name = "E_META_CHEST_FACING_XP",
				},
				{
					Name = "E_META_CHEST_FACING_ZM",
				},
				{
					Name = "E_META_CHEST_FACING_ZP",
				},
				{
					Name = "E_META_CONCRETE_BLACK",
				},
				{
					Name = "E_META_CONCRETE_BLUE",
				},
				{
					Name = "E_META_CONCRETE_BROWN",
				},
				{
					Name = "E_META_CONCRETE_CYAN",
				},
				{
					Name = "E_META_CONCRETE_GRAY",
				},
				{
					Name = "E_META_CONCRETE_GREEN",
				},
				{
					Name = "E_META_CONCRETE_LIGHTBLUE",
				},
				{
					Name = "E_META_CONCRETE_LIGHTGRAY",
				},
				{
					Name = "E_META_CONCRETE_LIGHTGREEN",
				},
				{
					Name = "E_META_CONCRETE_MAGENTA",
				},
				{
					Name = "E_META_CONCRETE_ORANGE",
				},
				{
					Name = "E_META_CONCRETE_PINK",
				},
				{
					Name = "E_META_CONCRETE_POWDER_BLACK",
				},
				{
					Name = "E_META_CONCRETE_POWDER_BLUE",
				},
				{
					Name = "E_META_CONCRETE_POWDER_BROWN",
				},
				{
					Name = "E_META_CONCRETE_POWDER_CYAN",
				},
				{
					Name = "E_META_CONCRETE_POWDER_GRAY",
				},
				{
					Name = "E_META_CONCRETE_POWDER_GREEN",
				},
				{
					Name = "E_META_CONCRETE_POWDER_LIGHTBLUE",
				},
				{
					Name = "E_META_CONCRETE_POWDER_LIGHTGRAY",
				},
				{
					Name = "E_META_CONCRETE_POWDER_LIGHTGREEN",
				},
				{
					Name = "E_META_CONCRETE_POWDER_MAGENTA",
				},
				{
					Name = "E_META_CONCRETE_POWDER_ORANGE",
				},
				{
					Name = "E_META_CONCRETE_POWDER_PINK",
				},
				{
					Name = "E_META_CONCRETE_POWDER_PURPLE",
				},
				{
					Name = "E_META_CONCRETE_POWDER_RED",
				},
				{
					Name = "E_META_CONCRETE_POWDER_WHITE",
				},
				{
					Name = "E_META_CONCRETE_POWDER_YELLOW",
				},
				{
					Name = "E_META_CONCRETE_PURPLE",
				},
				{
					Name = "E_META_CONCRETE_RED",
				},
				{
					Name = "E_META_CONCRETE_WHITE",
				},
				{
					Name = "E_META_CONCRETE_YELLOW",
				},
				{
					Name = "E_META_DIRT_COARSE",
				},
				{
					Name = "E_META_DIRT_GRASSLESS",
				},
				{
					Name = "E_META_DIRT_NORMAL",
				},
				{
					Name = "E_META_DIRT_PODZOL",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_BRICK",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_COBBLESTONE",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_NETHER_BRICK",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_QUARTZ",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_SANDSTON",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_SMOOTH_SANDSTONE",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_SMOOTH_STONE",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_STONE",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_STONE_BRICK",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_TILE_QUARTZ",
				},
				{
					Name = "E_META_DOUBLE_STONE_SLAB_WOODEN",
				},
				{
					Name = "E_META_DROPSPENSER_ACTIVATED",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_MASK",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_XM",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_XP",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_YM",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_YP",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_ZM",
				},
				{
					Name = "E_META_DROPSPENSER_FACING_ZP",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_EYE",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_NO_EYE",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_XM",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_XM_EYE",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_XP",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_XP_EYE",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_ZM",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_ZM_EYE",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_ZP",
				},
				{
					Name = "E_META_END_PORTAL_FRAME_ZP_EYE",
				},
				{
					Name = "E_META_FLOWER_ALLIUM",
				},
				{
					Name = "E_META_FLOWER_BLUE_ORCHID",
				},
				{
					Name = "E_META_FLOWER_ORANGE_TULIP",
				},
				{
					Name = "E_META_FLOWER_OXEYE_DAISY",
				},
				{
					Name = "E_META_FLOWER_PINK_TULIP",
				},
				{
					Name = "E_META_FLOWER_POPPY",
				},
				{
					Name = "E_META_FLOWER_RED_TULIP",
				},
				{
					Name = "E_META_FLOWER_WHITE_TULIP",
				},
				{
					Name = "E_META_HOPPER_FACING_XM",
				},
				{
					Name = "E_META_HOPPER_FACING_XP",
				},
				{
					Name = "E_META_HOPPER_FACING_YM",
				},
				{
					Name = "E_META_HOPPER_FACING_ZM",
				},
				{
					Name = "E_META_HOPPER_FACING_ZP",
				},
				{
					Name = "E_META_HOPPER_UNATTACHED",
				},
				{
					Name = "E_META_JUKEBOX_OFF",
				},
				{
					Name = "E_META_JUKEBOX_ON",
				},
				{
					Name = "E_META_LEAVES_APPLE",
				},
				{
					Name = "E_META_LEAVES_APPLE_CHECK_DECAY",
				},
				{
					Name = "E_META_LEAVES_APPLE_NO_DECAY",
				},
				{
					Name = "E_META_LEAVES_BIRCH",
				},
				{
					Name = "E_META_LEAVES_BIRCH_CHECK_DECAY",
				},
				{
					Name = "E_META_LEAVES_BIRCH_NO_DECAY",
				},
				{
					Name = "E_META_LEAVES_CONIFER",
				},
				{
					Name = "E_META_LEAVES_CONIFER_CHECK_DECAY",
				},
				{
					Name = "E_META_LEAVES_CONIFER_NO_DECAY",
				},
				{
					Name = "E_META_LEAVES_JUNGLE",
				},
				{
					Name = "E_META_LEAVES_JUNGLE_CHECK_DECAY",
				},
				{
					Name = "E_META_LEAVES_JUNGLE_NO_DECAY",
				},
				{
					Name = "E_META_LOG_APPLE",
				},
				{
					Name = "E_META_LOG_BIRCH",
				},
				{
					Name = "E_META_LOG_BIRCH_BARK_ONLY",
				},
				{
					Name = "E_META_LOG_BIRCH_UP_DOWN",
				},
				{
					Name = "E_META_LOG_BIRCH_X",
				},
				{
					Name = "E_META_LOG_BIRCH_Z",
				},
				{
					Name = "E_META_LOG_CONIFER",
				},
				{
					Name = "E_META_LOG_JUNGLE",
				},
				{
					Name = "E_META_LOG_JUNGLE_BARK_ONLY",
				},
				{
					Name = "E_META_LOG_JUNGLE_UP_DOWN",
				},
				{
					Name = "E_META_LOG_JUNGLE_X",
				},
				{
					Name = "E_META_LOG_JUNGLE_Z",
				},
				{
					Name = "E_META_LOG_OAK_BARK_ONLY",
				},
				{
					Name = "E_META_LOG_OAK_UP_DOWN",
				},
				{
					Name = "E_META_LOG_OAK_X",
				},
				{
					Name = "E_META_LOG_OAK_Z",
				},
				{
					Name = "E_META_LOG_SPRUCE_BARK_ONLY",
				},
				{
					Name = "E_META_LOG_SPRUCE_UP_DOWN",
				},
				{
					Name = "E_META_LOG_SPRUCE_X",
				},
				{
					Name = "E_META_LOG_SPRUCE_Z",
				},
				{
					Name = "E_META_NEWLEAVES_ACACIA",
				},
				{
					Name = "E_META_NEWLEAVES_ACACIA_CHECK_DECAY",
				},
				{
					Name = "E_META_NEWLEAVES_ACACIA_NO_DECAY",
				},
				{
					Name = "E_META_NEWLEAVES_DARK_OAK",
				},
				{
					Name = "E_META_NEWLEAVES_DARK_OAK_CHECK_DECAY",
				},
				{
					Name = "E_META_NEWLEAVES_DARK_OAK_NO_DECAY",
				},
				{
					Name = "E_META_NEWLOG_ACACIA_BARK_ONLY",
				},
				{
					Name = "E_META_NEWLOG_ACACIA_UP_DOWN",
				},
				{
					Name = "E_META_NEWLOG_ACACIA_X",
				},
				{
					Name = "E_META_NEWLOG_ACACIA_Z",
				},
				{
					Name = "E_META_NEWLOG_DARK_OAK_BARK_ONLY",
				},
				{
					Name = "E_META_NEWLOG_DARK_OAK_UP_DOWN",
				},
				{
					Name = "E_META_NEWLOG_DARK_OAK_X",
				},
				{
					Name = "E_META_NEWLOG_DARK_OAK_Z",
				},
				{
					Name = "E_META_NEW_LOG_ACACIA_WOOD",
				},
				{
					Name = "E_META_NEW_LOG_DARK_OAK_WOOD",
				},
				{
					Name = "E_META_PISTON_DOWN",
				},
				{
					Name = "E_META_PISTON_EXTENDED",
				},
				{
					Name = "E_META_PISTON_HEAD_STICKY",
				},
				{
					Name = "E_META_PISTON_U",
				},
				{
					Name = "E_META_PISTON_XM",
				},
				{
					Name = "E_META_PISTON_XP",
				},
				{
					Name = "E_META_PISTON_ZM",
				},
				{
					Name = "E_META_PISTON_ZP",
				},
				{
					Name = "E_META_PLANKS_ACACIA",
				},
				{
					Name = "E_META_PLANKS_BIRCH",
				},
				{
					Name = "E_META_PLANKS_DARK_OAK",
				},
				{
					Name = "E_META_PLANKS_JUNGLE",
				},
				{
					Name = "E_META_PLANKS_OAK",
				},
				{
					Name = "E_META_PLANKS_SPRUCE",
				},
				{
					Name = "E_META_PRESSURE_PLATE_DEPRESSED",
				},
				{
					Name = "E_META_PRESSURE_PLATE_RAISED",
				},
				{
					Name = "E_META_PRISMARINE_BLOCK_BRICKS",
				},
				{
					Name = "E_META_PRISMARINE_BLOCK_DARK",
				},
				{
					Name = "E_META_PRISMARINE_BLOCK_ROUGH",
				},
				{
					Name = "E_META_QUARTZ_CHISELLED",
				},
				{
					Name = "E_META_QUARTZ_NORMAL",
				},
				{
					Name = "E_META_QUARTZ_PILLAR",
				},
				{
					Name = "E_META_RAIL_ASCEND_XM",
				},
				{
					Name = "E_META_RAIL_ASCEND_XP",
				},
				{
					Name = "E_META_RAIL_ASCEND_ZM",
				},
				{
					Name = "E_META_RAIL_ASCEND_ZP",
				},
				{
					Name = "E_META_RAIL_CURVED_ZM_XM",
				},
				{
					Name = "E_META_RAIL_CURVED_ZM_XP",
				},
				{
					Name = "E_META_RAIL_CURVED_ZP_XM",
				},
				{
					Name = "E_META_RAIL_CURVED_ZP_XP",
				},
				{
					Name = "E_META_RAIL_XM_XP",
				},
				{
					Name = "E_META_RAIL_ZM_ZP",
				},
				{
					Name = "E_META_RED_SANDSTONE_NORMAL",
				},
				{
					Name = "E_META_RED_SANDSTONE_ORNAMENT",
				},
				{
					Name = "E_META_RED_SANDSTONE_SMOOTH",
				},
				{
					Name = "E_META_SANDSTONE_NORMAL",
				},
				{
					Name = "E_META_SANDSTONE_ORNAMENT",
				},
				{
					Name = "E_META_SANDSTONE_SMOOTH",
				},
				{
					Name = "E_META_SAND_NORMAL",
				},
				{
					Name = "E_META_SAND_RED",
				},
				{
					Name = "E_META_SAPLING_ACACIA",
				},
				{
					Name = "E_META_SAPLING_APPLE",
				},
				{
					Name = "E_META_SAPLING_BIRCH",
				},
				{
					Name = "E_META_SAPLING_CONIFER",
				},
				{
					Name = "E_META_SAPLING_DARK_OAK",
				},
				{
					Name = "E_META_SAPLING_JUNGLE",
				},
				{
					Name = "E_META_SILVERFISH_EGG_COBBLESTONE",
				},
				{
					Name = "E_META_SILVERFISH_EGG_STONE",
				},
				{
					Name = "E_META_SILVERFISH_EGG_STONE_BRICK",
				},
				{
					Name = "E_META_SNOW_LAYER_EIGHT",
				},
				{
					Name = "E_META_SNOW_LAYER_FIVE",
				},
				{
					Name = "E_META_SNOW_LAYER_FOUR",
				},
				{
					Name = "E_META_SNOW_LAYER_ONE",
				},
				{
					Name = "E_META_SNOW_LAYER_SEVEN",
				},
				{
					Name = "E_META_SNOW_LAYER_SIX",
				},
				{
					Name = "E_META_SNOW_LAYER_THREE",
				},
				{
					Name = "E_META_SNOW_LAYER_TWO",
				},
				{
					Name = "E_META_SPONGE_DRY",
				},
				{
					Name = "E_META_SPONGE_WET",
				},
				{
					Name = "E_META_STAINED_CLAY_BLACK",
				},
				{
					Name = "E_META_STAINED_CLAY_BLUE",
				},
				{
					Name = "E_META_STAINED_CLAY_BROWN",
				},
				{
					Name = "E_META_STAINED_CLAY_CYAN",
				},
				{
					Name = "E_META_STAINED_CLAY_GRAY",
				},
				{
					Name = "E_META_STAINED_CLAY_GREEN",
				},
				{
					Name = "E_META_STAINED_CLAY_LIGHTBLUE",
				},
				{
					Name = "E_META_STAINED_CLAY_LIGHTGRAY",
				},
				{
					Name = "E_META_STAINED_CLAY_LIGHTGREEN",
				},
				{
					Name = "E_META_STAINED_CLAY_MAGENTA",
				},
				{
					Name = "E_META_STAINED_CLAY_ORANGE",
				},
				{
					Name = "E_META_STAINED_CLAY_PINK",
				},
				{
					Name = "E_META_STAINED_CLAY_PURPLE",
				},
				{
					Name = "E_META_STAINED_CLAY_RED",
				},
				{
					Name = "E_META_STAINED_CLAY_WHITE",
				},
				{
					Name = "E_META_STAINED_CLAY_YELLOW",
				},
				{
					Name = "E_META_STAINED_GLASS_BLACK",
				},
				{
					Name = "E_META_STAINED_GLASS_BLUE",
				},
				{
					Name = "E_META_STAINED_GLASS_BROWN",
				},
				{
					Name = "E_META_STAINED_GLASS_CYAN",
				},
				{
					Name = "E_META_STAINED_GLASS_GRAY",
				},
				{
					Name = "E_META_STAINED_GLASS_GREEN",
				},
				{
					Name = "E_META_STAINED_GLASS_LIGHTBLUE",
				},
				{
					Name = "E_META_STAINED_GLASS_LIGHTGRAY",
				},
				{
					Name = "E_META_STAINED_GLASS_LIGHTGREEN",
				},
				{
					Name = "E_META_STAINED_GLASS_MAGENTA",
				},
				{
					Name = "E_META_STAINED_GLASS_ORANGE",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_BLACK",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_BLUE",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_BROWN",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_CYAN",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_GRAY",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_GREEN",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_LIGHTBLUE",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_LIGHTGRAY",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_LIGHTGREEN",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_MAGENTA",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_ORANGE",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_PINK",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_PURPLE",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_RED",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_WHITE",
				},
				{
					Name = "E_META_STAINED_GLASS_PANE_YELLOW",
				},
				{
					Name = "E_META_STAINED_GLASS_PINK",
				},
				{
					Name = "E_META_STAINED_GLASS_PURPLE",
				},
				{
					Name = "E_META_STAINED_GLASS_RED",
				},
				{
					Name = "E_META_STAINED_GLASS_WHITE",
				},
				{
					Name = "E_META_STAINED_GLASS_YELLOW",
				},
				{
					Name = "E_META_STONE_ANDESITE",
				},
				{
					Name = "E_META_STONE_BRICK_CRACKED",
				},
				{
					Name = "E_META_STONE_BRICK_MOSSY",
				},
				{
					Name = "E_META_STONE_BRICK_NORMAL",
				},
				{
					Name = "E_META_STONE_BRICK_ORNAMENT",
				},
				{
					Name = "E_META_STONE_DIORITE",
				},
				{
					Name = "E_META_STONE_GRANITE",
				},
				{
					Name = "E_META_STONE_POLISHED_ANDESITE",
				},
				{
					Name = "E_META_STONE_POLISHED_DIORITE",
				},
				{
					Name = "E_META_STONE_POLISHED_GRANITE",
				},
				{
					Name = "E_META_STONE_SLAB_BRICK",
				},
				{
					Name = "E_META_STONE_SLAB_COBBLESTONE",
				},
				{
					Name = "E_META_STONE_SLAB_NETHER_BRICK",
				},
				{
					Name = "E_META_STONE_SLAB_PLANKS",
				},
				{
					Name = "E_META_STONE_SLAB_QUARTZ",
				},
				{
					Name = "E_META_STONE_SLAB_SANDSTONE",
				},
				{
					Name = "E_META_STONE_SLAB_STONE",
				},
				{
					Name = "E_META_STONE_SLAB_STONE_BRICK",
				},
				{
					Name = "E_META_STONE_STONE",
				},
				{
					Name = "E_META_TALL_GRASS_BIOME",
				},
				{
					Name = "E_META_TALL_GRASS_DEAD_SHRUB",
				},
				{
					Name = "E_META_TALL_GRASS_FERN",
				},
				{
					Name = "E_META_TALL_GRASS_GRASS",
				},
				{
					Name = "E_META_TORCH_EAST",
				},
				{
					Name = "E_META_TORCH_FLOOR",
				},
				{
					Name = "E_META_TORCH_NORTH",
				},
				{
					Name = "E_META_TORCH_SOUTH",
				},
				{
					Name = "E_META_TORCH_WEST",
				},
				{
					Name = "E_META_TORCH_XM",
				},
				{
					Name = "E_META_TORCH_XP",
				},
				{
					Name = "E_META_TORCH_ZM",
				},
				{
					Name = "E_META_TORCH_ZP",
				},
				{
					Name = "E_META_WOODEN_DOUBLE_SLAB_ACACIA",
				},
				{
					Name = "E_META_WOODEN_DOUBLE_SLAB_BIRCH",
				},
				{
					Name = "E_META_WOODEN_DOUBLE_SLAB_DARK_OAK",
				},
				{
					Name = "E_META_WOODEN_DOUBLE_SLAB_JUNGLE",
				},
				{
					Name = "E_META_WOODEN_DOUBLE_SLAB_OAK",
				},
				{
					Name = "E_META_WOODEN_DOUBLE_SLAB_SPRUCE",
				},
				{
					Name = "E_META_WOODEN_SLAB_ACACIA",
				},
				{
					Name = "E_META_WOODEN_SLAB_BIRCH",
				},
				{
					Name = "E_META_WOODEN_SLAB_DARK_OAK",
				},
				{
					Name = "E_META_WOODEN_SLAB_JUNGLE",
				},
				{
					Name = "E_META_WOODEN_SLAB_OAK",
				},
				{
					Name = "E_META_WOODEN_SLAB_SPRUCE",
				},
				{
					Name = "E_META_WOODEN_SLAB_UPSIDE_DOWN",
				},
				{
					Name = "E_META_WOOL_BLACK",
				},
				{
					Name = "E_META_WOOL_BLUE",
				},
				{
					Name = "E_META_WOOL_BROWN",
				},
				{
					Name = "E_META_WOOL_CYAN",
				},
				{
					Name = "E_META_WOOL_GRAY",
				},
				{
					Name = "E_META_WOOL_GREEN",
				},
				{
					Name = "E_META_WOOL_LIGHTBLUE",
				},
				{
					Name = "E_META_WOOL_LIGHTGRAY",
				},
				{
					Name = "E_META_WOOL_LIGHTGREEN",
				},
				{
					Name = "E_META_WOOL_MAGENTA",
				},
				{
					Name = "E_META_WOOL_ORANGE",
				},
				{
					Name = "E_META_WOOL_PINK",
				},
				{
					Name = "E_META_WOOL_PURPLE",
				},
				{
					Name = "E_META_WOOL_RED",
				},
				{
					Name = "E_META_WOOL_WHITE",
				},
				{
					Name = "E_META_WOOL_YELLOW",
				},
			},
			ENUM_ITEM_ID =
			{
				{
					Name = "E_ITEM_11_DISC",
				},
				{
					Name = "E_ITEM_13_DISC",
				},
				{
					Name = "E_ITEM_ACACIA_BOAT",
				},
				{
					Name = "E_ITEM_ACACIA_DOOR",
				},
				{
					Name = "E_ITEM_ARMOR_STAND",
				},
				{
					Name = "E_ITEM_ARROW",
				},
				{
					Name = "E_ITEM_BAKED_POTATO",
				},
				{
					Name = "E_ITEM_BANNER",
				},
				{
					Name = "E_ITEM_BED",
				},
				{
					Name = "E_ITEM_BEETROOT",
				},
				{
					Name = "E_ITEM_BEETROOT_SEEDS",
				},
				{
					Name = "E_ITEM_BEETROOT_SOUP",
				},
				{
					Name = "E_ITEM_BIRCH_BOAT",
				},
				{
					Name = "E_ITEM_BIRCH_DOOR",
				},
				{
					Name = "E_ITEM_BLAZE_POWDER",
				},
				{
					Name = "E_ITEM_BLAZE_ROD",
				},
				{
					Name = "E_ITEM_BLOCKS_DISC",
				},
				{
					Name = "E_ITEM_BOAT",
				},
				{
					Name = "E_ITEM_BONE",
				},
				{
					Name = "E_ITEM_BOOK",
				},
				{
					Name = "E_ITEM_BOOK_AND_QUILL",
				},
				{
					Name = "E_ITEM_BOTTLE_O_ENCHANTING",
				},
				{
					Name = "E_ITEM_BOW",
				},
				{
					Name = "E_ITEM_BOWL",
				},
				{
					Name = "E_ITEM_BREAD",
				},
				{
					Name = "E_ITEM_BREWING_STAND",
				},
				{
					Name = "E_ITEM_BUCKET",
				},
				{
					Name = "E_ITEM_CAKE",
				},
				{
					Name = "E_ITEM_CARROT",
				},
				{
					Name = "E_ITEM_CARROT_ON_STICK",
				},
				{
					Name = "E_ITEM_CAT_DISC",
				},
				{
					Name = "E_ITEM_CAULDRON",
				},
				{
					Name = "E_ITEM_CHAIN_BOOTS",
				},
				{
					Name = "E_ITEM_CHAIN_CHESTPLATE",
				},
				{
					Name = "E_ITEM_CHAIN_HELMET",
				},
				{
					Name = "E_ITEM_CHAIN_LEGGINGS",
				},
				{
					Name = "E_ITEM_CHEST_MINECART",
				},
				{
					Name = "E_ITEM_CHIRP_DISC",
				},
				{
					Name = "E_ITEM_CHORUS_FRUIT",
				},
				{
					Name = "E_ITEM_CLAY",
				},
				{
					Name = "E_ITEM_CLAY_BRICK",
				},
				{
					Name = "E_ITEM_CLOCK",
				},
				{
					Name = "E_ITEM_COAL",
				},
				{
					Name = "E_ITEM_COMPARATOR",
				},
				{
					Name = "E_ITEM_COMPASS",
				},
				{
					Name = "E_ITEM_COOKED_CHICKEN",
				},
				{
					Name = "E_ITEM_COOKED_FISH",
				},
				{
					Name = "E_ITEM_COOKED_MUTTON",
				},
				{
					Name = "E_ITEM_COOKED_PORKCHOP",
				},
				{
					Name = "E_ITEM_COOKED_RABBIT",
				},
				{
					Name = "E_ITEM_COOKIE",
				},
				{
					Name = "E_ITEM_DARK_OAK_BOAT",
				},
				{
					Name = "E_ITEM_DARK_OAK_DOOR",
				},
				{
					Name = "E_ITEM_DIAMOND",
				},
				{
					Name = "E_ITEM_DIAMOND_AXE",
				},
				{
					Name = "E_ITEM_DIAMOND_BOOTS",
				},
				{
					Name = "E_ITEM_DIAMOND_CHESTPLATE",
				},
				{
					Name = "E_ITEM_DIAMOND_HELMET",
				},
				{
					Name = "E_ITEM_DIAMOND_HOE",
				},
				{
					Name = "E_ITEM_DIAMOND_HORSE_ARMOR",
				},
				{
					Name = "E_ITEM_DIAMOND_LEGGINGS",
				},
				{
					Name = "E_ITEM_DIAMOND_PICKAXE",
				},
				{
					Name = "E_ITEM_DIAMOND_SHOVEL",
				},
				{
					Name = "E_ITEM_DIAMOND_SWORD",
				},
				{
					Name = "E_ITEM_DRAGON_BREATH",
				},
				{
					Name = "E_ITEM_DYE",
				},
				{
					Name = "E_ITEM_EGG",
				},
				{
					Name = "E_ITEM_ELYTRA",
				},
				{
					Name = "E_ITEM_EMERALD",
				},
				{
					Name = "E_ITEM_EMPTY",
				},
				{
					Name = "E_ITEM_EMPTY_MAP",
				},
				{
					Name = "E_ITEM_ENCHANTED_BOOK",
				},
				{
					Name = "E_ITEM_ENDER_PEARL",
				},
				{
					Name = "E_ITEM_END_CRYSTAL",
				},
				{
					Name = "E_ITEM_EYE_OF_ENDER",
				},
				{
					Name = "E_ITEM_FAR_DISC",
				},
				{
					Name = "E_ITEM_FEATHER",
				},
				{
					Name = "E_ITEM_FERMENTED_SPIDER_EYE",
				},
				{
					Name = "E_ITEM_FIREWORK_ROCKET",
				},
				{
					Name = "E_ITEM_FIREWORK_STAR",
				},
				{
					Name = "E_ITEM_FIRE_CHARGE",
				},
				{
					Name = "E_ITEM_FIRST",
				},
				{
					Name = "E_ITEM_FIRST_DISC",
				},
				{
					Name = "E_ITEM_FISHING_ROD",
				},
				{
					Name = "E_ITEM_FLINT",
				},
				{
					Name = "E_ITEM_FLINT_AND_STEEL",
				},
				{
					Name = "E_ITEM_FLOWER_POT",
				},
				{
					Name = "E_ITEM_FURNACE_MINECART",
				},
				{
					Name = "E_ITEM_GHAST_TEAR",
				},
				{
					Name = "E_ITEM_GLASS_BOTTLE",
				},
				{
					Name = "E_ITEM_GLISTERING_MELON",
				},
				{
					Name = "E_ITEM_GLOWSTONE_DUST",
				},
				{
					Name = "E_ITEM_GOLD",
				},
				{
					Name = "E_ITEM_GOLDEN_APPLE",
				},
				{
					Name = "E_ITEM_GOLDEN_CARROT",
				},
				{
					Name = "E_ITEM_GOLD_AXE",
				},
				{
					Name = "E_ITEM_GOLD_BOOTS",
				},
				{
					Name = "E_ITEM_GOLD_CHESTPLATE",
				},
				{
					Name = "E_ITEM_GOLD_HELMET",
				},
				{
					Name = "E_ITEM_GOLD_HOE",
				},
				{
					Name = "E_ITEM_GOLD_HORSE_ARMOR",
				},
				{
					Name = "E_ITEM_GOLD_LEGGINGS",
				},
				{
					Name = "E_ITEM_GOLD_NUGGET",
				},
				{
					Name = "E_ITEM_GOLD_PICKAXE",
				},
				{
					Name = "E_ITEM_GOLD_SHOVEL",
				},
				{
					Name = "E_ITEM_GOLD_SWORD",
				},
				{
					Name = "E_ITEM_GUNPOWDER",
				},
				{
					Name = "E_ITEM_HEAD",
				},
				{
					Name = "E_ITEM_IRON",
				},
				{
					Name = "E_ITEM_IRON_AXE",
				},
				{
					Name = "E_ITEM_IRON_BOOTS",
				},
				{
					Name = "E_ITEM_IRON_CHESTPLATE",
				},
				{
					Name = "E_ITEM_IRON_DOOR",
				},
				{
					Name = "E_ITEM_IRON_HELMET",
				},
				{
					Name = "E_ITEM_IRON_HOE",
				},
				{
					Name = "E_ITEM_IRON_HORSE_ARMOR",
				},
				{
					Name = "E_ITEM_IRON_LEGGINGS",
				},
				{
					Name = "E_ITEM_IRON_NUGGET",
				},
				{
					Name = "E_ITEM_IRON_PICKAXE",
				},
				{
					Name = "E_ITEM_IRON_SHOVEL",
				},
				{
					Name = "E_ITEM_IRON_SWORD",
				},
				{
					Name = "E_ITEM_ITEM_FRAME",
				},
				{
					Name = "E_ITEM_JUNGLE_BOAT",
				},
				{
					Name = "E_ITEM_JUNGLE_DOOR",
				},
				{
					Name = "E_ITEM_LAST",
					Desc = "Maximum valid ItemType",
				},
				{
					Name = "E_ITEM_LAST_DISC",
					Desc = "Maximum disc itemtype number used",
				},
				{
					Name = "E_ITEM_LAST_DISC_PLUS_ONE",
					Desc = "Useless, really, but needs to be present for the following value",
				},
				{
					Name = "E_ITEM_LAVA_BUCKET",
				},
				{
					Name = "E_ITEM_LEAD",
				},
				{
					Name = "E_ITEM_LEASH",
				},
				{
					Name = "E_ITEM_LEATHER",
				},
				{
					Name = "E_ITEM_LEATHER_BOOTS",
				},
				{
					Name = "E_ITEM_LEATHER_CAP",
				},
				{
					Name = "E_ITEM_LEATHER_PANTS",
				},
				{
					Name = "E_ITEM_LEATHER_TUNIC",
				},
				{
					Name = "E_ITEM_LINGERING_POTION",
				},
				{
					Name = "E_ITEM_MAGMA_CREAM",
				},
				{
					Name = "E_ITEM_MALL_DISC",
				},
				{
					Name = "E_ITEM_MAP",
				},
				{
					Name = "E_ITEM_MAX_CONSECUTIVE_TYPE_ID",
					Desc = "Maximum consecutive ItemType number used",
				},
				{
					Name = "E_ITEM_MELLOHI_DISC",
				},
				{
					Name = "E_ITEM_MELON_SEEDS",
				},
				{
					Name = "E_ITEM_MELON_SLICE",
				},
				{
					Name = "E_ITEM_MILK",
				},
				{
					Name = "E_ITEM_MINECART",
				},
				{
					Name = "E_ITEM_MINECART_WITH_COMMAND_BLOCK",
				},
				{
					Name = "E_ITEM_MINECART_WITH_HOPPER",
				},
				{
					Name = "E_ITEM_MINECART_WITH_TNT",
				},
				{
					Name = "E_ITEM_MUSHROOM_SOUP",
				},
				{
					Name = "E_ITEM_NAME_TAG",
				},
				{
					Name = "E_ITEM_NETHER_BRICK",
				},
				{
					Name = "E_ITEM_NETHER_QUARTZ",
				},
				{
					Name = "E_ITEM_NETHER_STAR",
				},
				{
					Name = "E_ITEM_NETHER_WART",
				},
				{
					Name = "E_ITEM_NUMBER_OF_CONSECUTIVE_TYPES",
					Desc = "Number of individual (different) consecutive itemtypes",
				},
				{
					Name = "E_ITEM_PAINTING",
				},
				{
					Name = "E_ITEM_PAPER",
				},
				{
					Name = "E_ITEM_POISONOUS_POTATO",
				},
				{
					Name = "E_ITEM_POPPED_CHORUS_FRUIT",
				},
				{
					Name = "E_ITEM_POTATO",
				},
				{
					Name = "E_ITEM_POTION",
				},
				{
					Name = "E_ITEM_POTIONS",
				},
				{
					Name = "E_ITEM_PRISMARINE_CRYSTALS",
				},
				{
					Name = "E_ITEM_PRISMARINE_SHARD",
				},
				{
					Name = "E_ITEM_PUMPKIN_PIE",
				},
				{
					Name = "E_ITEM_PUMPKIN_SEEDS",
				},
				{
					Name = "E_ITEM_RABBITS_FOOT",
				},
				{
					Name = "E_ITEM_RABBIT_HIDE",
				},
				{
					Name = "E_ITEM_RABBIT_STEW",
				},
				{
					Name = "E_ITEM_RAW_BEEF",
				},
				{
					Name = "E_ITEM_RAW_CHICKEN",
				},
				{
					Name = "E_ITEM_RAW_FISH",
				},
				{
					Name = "E_ITEM_RAW_MUTTON",
				},
				{
					Name = "E_ITEM_RAW_PORKCHOP",
				},
				{
					Name = "E_ITEM_RAW_RABBIT",
				},
				{
					Name = "E_ITEM_REDSTONE_DUST",
				},
				{
					Name = "E_ITEM_REDSTONE_REPEATER",
				},
				{
					Name = "E_ITEM_RED_APPLE",
				},
				{
					Name = "E_ITEM_ROTTEN_FLESH",
				},
				{
					Name = "E_ITEM_SADDLE",
				},
				{
					Name = "E_ITEM_SEEDS",
				},
				{
					Name = "E_ITEM_SHEARS",
				},
				{
					Name = "E_ITEM_SHIELD",
				},
				{
					Name = "E_ITEM_SHULKER_SHELL",
				},
				{
					Name = "E_ITEM_SIGN",
				},
				{
					Name = "E_ITEM_SLIMEBALL",
				},
				{
					Name = "E_ITEM_SNOWBALL",
				},
				{
					Name = "E_ITEM_SPAWN_EGG",
				},
				{
					Name = "E_ITEM_SPECTRAL_ARROW",
				},
				{
					Name = "E_ITEM_SPIDER_EYE",
				},
				{
					Name = "E_ITEM_SPLASH_POTION",
				},
				{
					Name = "E_ITEM_SPRUCE_BOAT",
				},
				{
					Name = "E_ITEM_SPRUCE_DOOR",
				},
				{
					Name = "E_ITEM_STAL_DISC",
				},
				{
					Name = "E_ITEM_STEAK",
				},
				{
					Name = "E_ITEM_STICK",
				},
				{
					Name = "E_ITEM_STONE_AXE",
				},
				{
					Name = "E_ITEM_STONE_HOE",
				},
				{
					Name = "E_ITEM_STONE_PICKAXE",
				},
				{
					Name = "E_ITEM_STONE_SHOVEL",
				},
				{
					Name = "E_ITEM_STONE_SWORD",
				},
				{
					Name = "E_ITEM_STRAD_DISC",
				},
				{
					Name = "E_ITEM_STRING",
				},
				{
					Name = "E_ITEM_SUGAR",
				},
				{
					Name = "E_ITEM_SUGARCANE",
				},
				{
					Name = "E_ITEM_SUGAR_CANE",
				},
				{
					Name = "E_ITEM_TIPPED_ARROW",
				},
				{
					Name = "E_ITEM_TOTEM_OF_UNDYING",
				},
				{
					Name = "E_ITEM_WAIT_DISC",
				},
				{
					Name = "E_ITEM_WARD_DISC",
				},
				{
					Name = "E_ITEM_WATER_BUCKET",
				},
				{
					Name = "E_ITEM_WHEAT",
				},
				{
					Name = "E_ITEM_WOODEN_AXE",
				},
				{
					Name = "E_ITEM_WOODEN_DOOR",
				},
				{
					Name = "E_ITEM_WOODEN_HOE",
				},
				{
					Name = "E_ITEM_WOODEN_PICKAXE",
				},
				{
					Name = "E_ITEM_WOODEN_SHOVEL",
				},
				{
					Name = "E_ITEM_WOODEN_SWORD",
				},
				{
					Name = "E_ITEM_WRITTEN_BOOK",
				},
			},
			ENUM_ITEM_META =
			{
				{
					Name = "E_META_BANNER_BLACK",
				},
				{
					Name = "E_META_BANNER_BLUE",
				},
				{
					Name = "E_META_BANNER_BROWN",
				},
				{
					Name = "E_META_BANNER_CYAN",
				},
				{
					Name = "E_META_BANNER_GRAY",
				},
				{
					Name = "E_META_BANNER_GREEN",
				},
				{
					Name = "E_META_BANNER_LIGHTBLUE",
				},
				{
					Name = "E_META_BANNER_LIGHTGRAY",
				},
				{
					Name = "E_META_BANNER_LIGHTGREEN",
				},
				{
					Name = "E_META_BANNER_MAGENTA",
				},
				{
					Name = "E_META_BANNER_ORANGE",
				},
				{
					Name = "E_META_BANNER_PINK",
				},
				{
					Name = "E_META_BANNER_PURPLE",
				},
				{
					Name = "E_META_BANNER_RED",
				},
				{
					Name = "E_META_BANNER_WHITE",
				},
				{
					Name = "E_META_BANNER_YELLOW",
				},
				{
					Name = "E_META_COAL_CHARCOAL",
				},
				{
					Name = "E_META_COAL_NORMAL",
				},
				{
					Name = "E_META_COOKED_FISH_FISH",
				},
				{
					Name = "E_META_COOKED_FISH_SALMON",
				},
				{
					Name = "E_META_DYE_BLACK",
				},
				{
					Name = "E_META_DYE_BLUE",
				},
				{
					Name = "E_META_DYE_BROWN",
				},
				{
					Name = "E_META_DYE_CYAN",
				},
				{
					Name = "E_META_DYE_GRAY",
				},
				{
					Name = "E_META_DYE_GREEN",
				},
				{
					Name = "E_META_DYE_LIGHTBLUE",
				},
				{
					Name = "E_META_DYE_LIGHTGRAY",
				},
				{
					Name = "E_META_DYE_LIGHTGREEN",
				},
				{
					Name = "E_META_DYE_MAGENTA",
				},
				{
					Name = "E_META_DYE_ORANGE",
				},
				{
					Name = "E_META_DYE_PINK",
				},
				{
					Name = "E_META_DYE_PURPLE",
				},
				{
					Name = "E_META_DYE_RED",
				},
				{
					Name = "E_META_DYE_WHITE",
				},
				{
					Name = "E_META_DYE_YELLOW",
				},
				{
					Name = "E_META_GOLDEN_APPLE_ENCHANTED",
				},
				{
					Name = "E_META_GOLDEN_APPLE_NORMAL",
				},
				{
					Name = "E_META_HEAD_CREEPER",
				},
				{
					Name = "E_META_HEAD_DRAGON",
				},
				{
					Name = "E_META_HEAD_PLAYER",
				},
				{
					Name = "E_META_HEAD_SKELETON",
				},
				{
					Name = "E_META_HEAD_WITHER",
				},
				{
					Name = "E_META_HEAD_ZOMBIE",
				},
				{
					Name = "E_META_RAW_FISH_CLOWNFISH",
				},
				{
					Name = "E_META_RAW_FISH_FISH",
				},
				{
					Name = "E_META_RAW_FISH_PUFFERFISH",
				},
				{
					Name = "E_META_RAW_FISH_SALMON",
				},
				{
					Name = "E_META_SPAWN_EGG_ARROW",
				},
				{
					Name = "E_META_SPAWN_EGG_BAT",
				},
				{
					Name = "E_META_SPAWN_EGG_BLAZE",
				},
				{
					Name = "E_META_SPAWN_EGG_BOAT",
				},
				{
					Name = "E_META_SPAWN_EGG_CAVE_SPIDER",
				},
				{
					Name = "E_META_SPAWN_EGG_CHICKEN",
				},
				{
					Name = "E_META_SPAWN_EGG_COW",
				},
				{
					Name = "E_META_SPAWN_EGG_CREEPER",
				},
				{
					Name = "E_META_SPAWN_EGG_ENDERMAN",
				},
				{
					Name = "E_META_SPAWN_EGG_ENDER_CRYSTAL",
				},
				{
					Name = "E_META_SPAWN_EGG_ENDER_DRAGON",
				},
				{
					Name = "E_META_SPAWN_EGG_ENDER_PEARL",
				},
				{
					Name = "E_META_SPAWN_EGG_EXPERIENCE_ORB",
				},
				{
					Name = "E_META_SPAWN_EGG_EXP_BOTTLE",
				},
				{
					Name = "E_META_SPAWN_EGG_EYE_OF_ENDER",
				},
				{
					Name = "E_META_SPAWN_EGG_FALLING_BLOCK",
				},
				{
					Name = "E_META_SPAWN_EGG_FIREBALL",
				},
				{
					Name = "E_META_SPAWN_EGG_FIREWORK",
				},
				{
					Name = "E_META_SPAWN_EGG_GHAST",
				},
				{
					Name = "E_META_SPAWN_EGG_GIANT",
				},
				{
					Name = "E_META_SPAWN_EGG_GUARDIAN",
				},
				{
					Name = "E_META_SPAWN_EGG_HORSE",
				},
				{
					Name = "E_META_SPAWN_EGG_IRON_GOLEM",
				},
				{
					Name = "E_META_SPAWN_EGG_ITEM_FRAME",
				},
				{
					Name = "E_META_SPAWN_EGG_LEASH_KNOT",
				},
				{
					Name = "E_META_SPAWN_EGG_MAGMA_CUBE",
				},
				{
					Name = "E_META_SPAWN_EGG_MINECART",
				},
				{
					Name = "E_META_SPAWN_EGG_MINECART_CHEST",
				},
				{
					Name = "E_META_SPAWN_EGG_MINECART_FURNACE",
				},
				{
					Name = "E_META_SPAWN_EGG_MINECART_HOPPER",
				},
				{
					Name = "E_META_SPAWN_EGG_MINECART_SPAWNER",
				},
				{
					Name = "E_META_SPAWN_EGG_MINECART_TNT",
				},
				{
					Name = "E_META_SPAWN_EGG_MOOSHROOM",
				},
				{
					Name = "E_META_SPAWN_EGG_OCELOT",
				},
				{
					Name = "E_META_SPAWN_EGG_PAINTING",
				},
				{
					Name = "E_META_SPAWN_EGG_PICKUP",
				},
				{
					Name = "E_META_SPAWN_EGG_PIG",
				},
				{
					Name = "E_META_SPAWN_EGG_PRIMED_TNT",
				},
				{
					Name = "E_META_SPAWN_EGG_RABBIT",
				},
				{
					Name = "E_META_SPAWN_EGG_SHEEP",
				},
				{
					Name = "E_META_SPAWN_EGG_SILVERFISH",
				},
				{
					Name = "E_META_SPAWN_EGG_SKELETON",
				},
				{
					Name = "E_META_SPAWN_EGG_SLIME",
				},
				{
					Name = "E_META_SPAWN_EGG_SMALL_FIREBALL",
				},
				{
					Name = "E_META_SPAWN_EGG_SNOWBALL",
				},
				{
					Name = "E_META_SPAWN_EGG_SNOW_GOLEM",
				},
				{
					Name = "E_META_SPAWN_EGG_SPIDER",
				},
				{
					Name = "E_META_SPAWN_EGG_SPLASH_POTION",
				},
				{
					Name = "E_META_SPAWN_EGG_SQUID",
				},
				{
					Name = "E_META_SPAWN_EGG_VILLAGER",
				},
				{
					Name = "E_META_SPAWN_EGG_WITCH",
				},
				{
					Name = "E_META_SPAWN_EGG_WITHER",
				},
				{
					Name = "E_META_SPAWN_EGG_WITHER_SKULL",
				},
				{
					Name = "E_META_SPAWN_EGG_WOLF",
				},
				{
					Name = "E_META_SPAWN_EGG_ZOMBIE",
				},
				{
					Name = "E_META_SPAWN_EGG_ZOMBIE_PIGMAN",
				},
				{
					Name = "E_META_TRACKS_X",
				},
				{
					Name = "E_META_TRACKS_Z",
				},
			},
			EffectID =
			{
				{
					Name = "PARTICLE_BLOCK_BREAK",
				},
				{
					Name = "PARTICLE_DRAGON_BREATH",
				},
				{
					Name = "PARTICLE_ENDERDRAGON_GROWL",
				},
				{
					Name = "PARTICLE_END_GATEWAY_SPAWN",
				},
				{
					Name = "PARTICLE_EYE_OF_ENDER",
				},
				{
					Name = "PARTICLE_HAPPY_VILLAGER",
				},
				{
					Name = "PARTICLE_MOBSPAWN",
				},
				{
					Name = "PARTICLE_SMOKE",
				},
				{
					Name = "PARTICLE_SPLASH_POTION",
				},
				{
					Name = "SFX_MOB_BAT_TAKEOFF",
				},
				{
					Name = "SFX_MOB_BLAZE_SHOOT",
				},
				{
					Name = "SFX_MOB_ENDERDRAGON_DEATH",
				},
				{
					Name = "SFX_MOB_ENDERDRAGON_SHOOT",
				},
				{
					Name = "SFX_MOB_GHAST_SHOOT",
				},
				{
					Name = "SFX_MOB_GHAST_WARN",
				},
				{
					Name = "SFX_MOB_WITHER_BREAK_BLOCK",
				},
				{
					Name = "SFX_MOB_WITHER_SHOOT",
				},
				{
					Name = "SFX_MOB_WITHER_SPAWN",
				},
				{
					Name = "SFX_MOB_ZOMBIE_INFECT",
				},
				{
					Name = "SFX_MOB_ZOMBIE_METAL",
				},
				{
					Name = "SFX_MOB_ZOMBIE_UNFECT",
				},
				{
					Name = "SFX_MOB_ZOMBIE_WOOD",
				},
				{
					Name = "SFX_MOB_ZOMBIE_WOOD_BREAK",
				},
				{
					Name = "SFX_RANDOM_ANVIL_BREAK",
				},
				{
					Name = "SFX_RANDOM_ANVIL_LAND",
				},
				{
					Name = "SFX_RANDOM_ANVIL_USE",
				},
				{
					Name = "SFX_RANDOM_BREWING_STAND_BREW",
				},
				{
					Name = "SFX_RANDOM_CHORUS_FLOWER_DEATH",
				},
				{
					Name = "SFX_RANDOM_CHORUS_FLOWER_GROW",
				},
				{
					Name = "SFX_RANDOM_DISPENSER_DISPENSE",
				},
				{
					Name = "SFX_RANDOM_DISPENSER_DISPENSE_FAIL",
				},
				{
					Name = "SFX_RANDOM_DISPENSER_SHOOT",
				},
				{
					Name = "SFX_RANDOM_ENDER_EYE_LAUNCH",
				},
				{
					Name = "SFX_RANDOM_FENCE_GATE_CLOSE",
				},
				{
					Name = "SFX_RANDOM_FENCE_GATE_OPEN",
				},
				{
					Name = "SFX_RANDOM_FIREWORK_SHOT",
				},
				{
					Name = "SFX_RANDOM_FIRE_EXTINGUISH",
				},
				{
					Name = "SFX_RANDOM_IRON_DOOR_CLOSE",
				},
				{
					Name = "SFX_RANDOM_IRON_DOOR_OPEN",
				},
				{
					Name = "SFX_RANDOM_IRON_TRAPDOOR_CLOSE",
				},
				{
					Name = "SFX_RANDOM_IRON_TRAPDOOR_OPEN",
				},
				{
					Name = "SFX_RANDOM_PLAY_MUSIC_DISC",
				},
				{
					Name = "SFX_RANDOM_PORTAL_TRAVEL",
				},
				{
					Name = "SFX_RANDOM_WOODEN_DOOR_CLOSE",
				},
				{
					Name = "SFX_RANDOM_WOODEN_DOOR_OPEN",
				},
				{
					Name = "SFX_RANDOM_WOODEN_TRAPDOOR_CLOSE",
				},
				{
					Name = "SFX_RANDOM_WOODEN_TRAPDOOR_OPEN",
				},
			},
			SmokeDirection =
			{
				{
					Name = "CENTRE",
				},
				{
					Name = "EAST",
				},
				{
					Name = "NORTH",
				},
				{
					Name = "NORTH_EAST",
				},
				{
					Name = "NORTH_WEST",
				},
				{
					Name = "SOUTH",
				},
				{
					Name = "SOUTH_EAST",
				},
				{
					Name = "SOUTH_WEST",
				},
				{
					Name = "WEST",
				},
			},
			eBlockFace =
			{
				{
					Name = "BLOCK_FACE_BOTTOM",
				},
				{
					Name = "BLOCK_FACE_EAST",
				},
				{
					Name = "BLOCK_FACE_MAX",
				},
				{
					Name = "BLOCK_FACE_MIN",
				},
				{
					Name = "BLOCK_FACE_NONE",
				},
				{
					Name = "BLOCK_FACE_NORTH",
				},
				{
					Name = "BLOCK_FACE_SOUTH",
				},
				{
					Name = "BLOCK_FACE_TOP",
				},
				{
					Name = "BLOCK_FACE_WEST",
				},
				{
					Name = "BLOCK_FACE_XM",
				},
				{
					Name = "BLOCK_FACE_XP",
				},
				{
					Name = "BLOCK_FACE_YM",
				},
				{
					Name = "BLOCK_FACE_YP",
				},
				{
					Name = "BLOCK_FACE_ZM",
				},
				{
					Name = "BLOCK_FACE_ZP",
				},
			},
			eChatType =
			{
				{
					Name = "ctAboveActionBar",
				},
				{
					Name = "ctChatBox",
				},
				{
					Name = "ctSystem",
				},
			},
			eClickAction =
			{
				{
					Name = "caCtrlDropKey",
				},
				{
					Name = "caDblClick",
				},
				{
					Name = "caDropKey",
				},
				{
					Name = "caLeftClick",
				},
				{
					Name = "caLeftClickOutside",
				},
				{
					Name = "caLeftClickOutsideHoldNothing",
				},
				{
					Name = "caLeftPaintBegin",
				},
				{
					Name = "caLeftPaintEnd",
				},
				{
					Name = "caLeftPaintProgress",
				},
				{
					Name = "caMiddleClick",
				},
				{
					Name = "caMiddlePaintBegin",
				},
				{
					Name = "caMiddlePaintEnd",
				},
				{
					Name = "caMiddlePaintProgress",
				},
				{
					Name = "caNumber1",
				},
				{
					Name = "caNumber2",
				},
				{
					Name = "caNumber3",
				},
				{
					Name = "caNumber4",
				},
				{
					Name = "caNumber5",
				},
				{
					Name = "caNumber6",
				},
				{
					Name = "caNumber7",
				},
				{
					Name = "caNumber8",
				},
				{
					Name = "caNumber9",
				},
				{
					Name = "caRightClick",
				},
				{
					Name = "caRightClickOutside",
				},
				{
					Name = "caRightClickOutsideHoldNothing",
				},
				{
					Name = "caRightPaintBegin",
				},
				{
					Name = "caRightPaintEnd",
				},
				{
					Name = "caRightPaintProgress",
				},
				{
					Name = "caShiftLeftClick",
				},
				{
					Name = "caShiftRightClick",
				},
				{
					Name = "caUnknown",
				},
			},
			eDamageType =
			{
				{
					Name = "dtAdmin",
				},
				{
					Name = "dtArrow",
				},
				{
					Name = "dtArrowAttack",
				},
				{
					Name = "dtAttack",
				},
				{
					Name = "dtBurning",
				},
				{
					Name = "dtCacti",
				},
				{
					Name = "dtCactus",
				},
				{
					Name = "dtCactusContact",
				},
				{
					Name = "dtCactuses",
				},
				{
					Name = "dtDrown",
				},
				{
					Name = "dtDrowning",
				},
				{
					Name = "dtEnderPearl",
				},
				{
					Name = "dtEntityAttack",
				},
				{
					Name = "dtExplosion",
				},
				{
					Name = "dtFall",
				},
				{
					Name = "dtFalling",
				},
				{
					Name = "dtFireContact",
				},
				{
					Name = "dtHunger",
				},
				{
					Name = "dtInFire",
				},
				{
					Name = "dtInVoid",
				},
				{
					Name = "dtLava",
				},
				{
					Name = "dtLavaContact",
				},
				{
					Name = "dtLightning",
				},
				{
					Name = "dtMob",
				},
				{
					Name = "dtMobAttack",
				},
				{
					Name = "dtOnFire",
				},
				{
					Name = "dtPawnAttack",
				},
				{
					Name = "dtPlugin",
				},
				{
					Name = "dtPoison",
				},
				{
					Name = "dtPoisoning",
				},
				{
					Name = "dtPotionOfHarming",
				},
				{
					Name = "dtProjectile",
				},
				{
					Name = "dtRangedAttack",
				},
				{
					Name = "dtStarvation",
				},
				{
					Name = "dtStarving",
				},
				{
					Name = "dtSuffocating",
				},
				{
					Name = "dtSuffocation",
				},
				{
					Name = "dtWither",
				},
				{
					Name = "dtWithering",
				},
			},
			eDimension =
			{
				{
					Name = "dimEnd",
				},
				{
					Name = "dimNether",
				},
				{
					Name = "dimNotSet",
				},
				{
					Name = "dimOverworld",
				},
			},
			eExplosionSource =
			{
				{
					Name = "esBed",
				},
				{
					Name = "esEnderCrystal",
				},
				{
					Name = "esGhastFireball",
				},
				{
					Name = "esMax",
				},
				{
					Name = "esMonster",
				},
				{
					Name = "esOther",
				},
				{
					Name = "esPlugin",
				},
				{
					Name = "esPrimedTNT",
				},
				{
					Name = "esWitherBirth",
				},
				{
					Name = "esWitherSkull",
				},
			},
			eGameMode =
			{
				{
					Name = "eGameMode_Adventure",
				},
				{
					Name = "eGameMode_Creative",
				},
				{
					Name = "eGameMode_NotSet",
				},
				{
					Name = "eGameMode_Spectator",
				},
				{
					Name = "eGameMode_Survival",
				},
				{
					Name = "gmAdventure",
				},
				{
					Name = "gmCreative",
				},
				{
					Name = "gmMax",
				},
				{
					Name = "gmMin",
				},
				{
					Name = "gmNotSet",
				},
				{
					Name = "gmSpectator",
				},
				{
					Name = "gmSurvival",
				},
			},
			eHand =
			{
				{
					Name = "hMain",
				},
				{
					Name = "hOff",
				},
			},
			eMainHand =
			{
				{
					Name = "mhLeft",
				},
				{
					Name = "mhRight",
				},
			},
			eMessageType =
			{
				{
					Name = "mtCustom",
				},
				{
					Name = "mtDeath",
				},
				{
					Name = "mtError",
				},
				{
					Name = "mtFail",
				},
				{
					Name = "mtFailure",
				},
				{
					Name = "mtFatal",
				},
				{
					Name = "mtInfo",
				},
				{
					Name = "mtInformation",
				},
				{
					Name = "mtJoin",
				},
				{
					Name = "mtLeave",
				},
				{
					Name = "mtMaxPlusOne",
				},
				{
					Name = "mtPM",
				},
				{
					Name = "mtPrivateMessage",
				},
				{
					Name = "mtSuccess",
				},
				{
					Name = "mtWarning",
				},
			},
			eMobHeadRotation =
			{
				{
					Name = "SKULL_ROTATION_EAST",
				},
				{
					Name = "SKULL_ROTATION_EAST_NORTH_EAST",
				},
				{
					Name = "SKULL_ROTATION_EAST_SOUTH_EAST",
				},
				{
					Name = "SKULL_ROTATION_NORTH",
				},
				{
					Name = "SKULL_ROTATION_NORTH_EAST",
				},
				{
					Name = "SKULL_ROTATION_NORTH_NORTH_EAST",
				},
				{
					Name = "SKULL_ROTATION_NORTH_NORTH_WEST",
				},
				{
					Name = "SKULL_ROTATION_NORTH_WEST",
				},
				{
					Name = "SKULL_ROTATION_SOUTH",
				},
				{
					Name = "SKULL_ROTATION_SOUTH_EAST",
				},
				{
					Name = "SKULL_ROTATION_SOUTH_SOUTH_EAST",
				},
				{
					Name = "SKULL_ROTATION_SOUTH_SOUTH_WEST",
				},
				{
					Name = "SKULL_ROTATION_SOUTH_WEST",
				},
				{
					Name = "SKULL_ROTATION_WEST",
				},
				{
					Name = "SKULL_ROTATION_WEST_NORTH_WEST",
				},
				{
					Name = "SKULL_ROTATION_WEST_SOUTH_WEST",
				},
			},
			eMobHeadType =
			{
				{
					Name = "SKULL_TYPE_CREEPER",
				},
				{
					Name = "SKULL_TYPE_DRAGON",
				},
				{
					Name = "SKULL_TYPE_PLAYER",
				},
				{
					Name = "SKULL_TYPE_SKELETON",
				},
				{
					Name = "SKULL_TYPE_WITHER",
				},
				{
					Name = "SKULL_TYPE_ZOMBIE",
				},
			},
			eMonsterType =
			{
				{
					Name = "mtBat",
				},
				{
					Name = "mtBlaze",
				},
				{
					Name = "mtCaveSpider",
				},
				{
					Name = "mtChicken",
				},
				{
					Name = "mtCow",
				},
				{
					Name = "mtCreeper",
				},
				{
					Name = "mtEnderDragon",
				},
				{
					Name = "mtEnderman",
				},
				{
					Name = "mtGhast",
				},
				{
					Name = "mtGiant",
				},
				{
					Name = "mtGuardian",
				},
				{
					Name = "mtHorse",
				},
				{
					Name = "mtInvalidType",
				},
				{
					Name = "mtIronGolem",
				},
				{
					Name = "mtMagmaCube",
				},
				{
					Name = "mtMax",
				},
				{
					Name = "mtMooshroom",
				},
				{
					Name = "mtOcelot",
				},
				{
					Name = "mtPig",
				},
				{
					Name = "mtRabbit",
				},
				{
					Name = "mtSheep",
				},
				{
					Name = "mtSilverfish",
				},
				{
					Name = "mtSkeleton",
				},
				{
					Name = "mtSlime",
				},
				{
					Name = "mtSnowGolem",
				},
				{
					Name = "mtSpider",
				},
				{
					Name = "mtSquid",
				},
				{
					Name = "mtVillager",
				},
				{
					Name = "mtWitch",
				},
				{
					Name = "mtWither",
				},
				{
					Name = "mtWolf",
				},
				{
					Name = "mtZombie",
				},
				{
					Name = "mtZombiePigman",
				},
			},
			eShrapnelLevel =
			{
				{
					Name = "slAll",
				},
				{
					Name = "slGravityAffectedOnly",
				},
				{
					Name = "slNone",
				},
			},
			eSkinPart =
			{
				{
					Name = "spCape",
				},
				{
					Name = "spHat",
				},
				{
					Name = "spJacket",
				},
				{
					Name = "spLeftPants",
				},
				{
					Name = "spLeftSleeve",
				},
				{
					Name = "spMask",
				},
				{
					Name = "spRightPants",
				},
				{
					Name = "spRightSleeve",
				},
			},
			eSpreadSource =
			{
				{
					Name = "ssFireSpread",
				},
				{
					Name = "ssGrassSpread",
				},
				{
					Name = "ssMushroomSpread",
				},
				{
					Name = "ssMycelSpread",
				},
				{
					Name = "ssVineSpread",
				},
			},
			eStatistic =
			{
				{
					Name = "achAcquireIron",
				},
				{
					Name = "achBakeCake",
				},
				{
					Name = "achBlazeRod",
				},
				{
					Name = "achBookshelf",
				},
				{
					Name = "achBreedCow",
				},
				{
					Name = "achBrewPotion",
				},
				{
					Name = "achCookFish",
				},
				{
					Name = "achCraftBetterPick",
				},
				{
					Name = "achCraftEnchantTable",
				},
				{
					Name = "achCraftFurnace",
				},
				{
					Name = "achCraftHoe",
				},
				{
					Name = "achCraftPickaxe",
				},
				{
					Name = "achCraftSword",
				},
				{
					Name = "achCraftWorkbench",
				},
				{
					Name = "achDefeatDragon",
				},
				{
					Name = "achDiamonds",
				},
				{
					Name = "achEnterPortal",
				},
				{
					Name = "achEnterTheEnd",
				},
				{
					Name = "achExploreAllBiomes",
				},
				{
					Name = "achFlyPig",
				},
				{
					Name = "achFullBeacon",
				},
				{
					Name = "achKillCow",
				},
				{
					Name = "achKillMonster",
				},
				{
					Name = "achKillWither",
				},
				{
					Name = "achMakeBread",
				},
				{
					Name = "achMineWood",
				},
				{
					Name = "achOnARail",
				},
				{
					Name = "achOpenInv",
				},
				{
					Name = "achOverkill",
				},
				{
					Name = "achReturnToSender",
				},
				{
					Name = "achSnipeSkeleton",
				},
				{
					Name = "achSpawnWither",
				},
				{
					Name = "achThrowDiamonds",
				},
				{
					Name = "statAnimalsBred",
				},
				{
					Name = "statCount",
				},
				{
					Name = "statDamageDealt",
				},
				{
					Name = "statDamageTaken",
				},
				{
					Name = "statDeaths",
				},
				{
					Name = "statDistBoat",
				},
				{
					Name = "statDistClimbed",
				},
				{
					Name = "statDistDove",
				},
				{
					Name = "statDistFallen",
				},
				{
					Name = "statDistFlown",
				},
				{
					Name = "statDistHorse",
				},
				{
					Name = "statDistMinecart",
				},
				{
					Name = "statDistPig",
				},
				{
					Name = "statDistSwum",
				},
				{
					Name = "statDistWalked",
				},
				{
					Name = "statFishCaught",
				},
				{
					Name = "statGamesQuit",
				},
				{
					Name = "statInvalid",
				},
				{
					Name = "statItemsDropped",
				},
				{
					Name = "statJumps",
				},
				{
					Name = "statJunkFished",
				},
				{
					Name = "statMinutesPlayed",
				},
				{
					Name = "statMobKills",
				},
				{
					Name = "statPlayerKills",
				},
				{
					Name = "statTreasureFished",
				},
			},
			eWeather =
			{
				{
					Name = "eWeather_Rain",
				},
				{
					Name = "eWeather_Sunny",
				},
				{
					Name = "eWeather_ThunderStorm",
				},
				{
					Name = "wRain",
				},
				{
					Name = "wStorm",
				},
				{
					Name = "wSunny",
				},
				{
					Name = "wThunderstorm",
				},
			},
		},
	},
}
