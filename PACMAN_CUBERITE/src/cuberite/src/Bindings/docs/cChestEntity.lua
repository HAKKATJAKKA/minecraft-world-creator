return
{
	cChestEntity =
	{
		Inherits =
		{
			"cBlockEntityWithItems",
		},
		Enums =
		{
			unnamedEnum_1 =
			{
				{
					Name = "ContentsHeight",
				},
				{
					Name = "ContentsWidth",
				},
			},
		},
	},
}
