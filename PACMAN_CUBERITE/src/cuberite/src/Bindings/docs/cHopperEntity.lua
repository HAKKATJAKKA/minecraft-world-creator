return
{
	cHopperEntity =
	{
		Inherits =
		{
			"cBlockEntityWithItems",
		},
		Enums =
		{
			unnamedEnum_1 =
			{
				{
					Name = "ContentsHeight",
				},
				{
					Name = "ContentsWidth",
				},
				{
					Name = "TICKS_PER_TRANSFER",
					Desc = "How many ticks at minimum between two item transfers to or from the hopper",
				},
			},
		},
	},
}
